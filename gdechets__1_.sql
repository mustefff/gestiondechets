-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 27 juil. 2023 à 10:04
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gdechets`
--

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(39, '2014_10_12_000000_create_users_table', 2),
(40, '2014_10_12_100000_create_password_reset_tokens_table', 2),
(41, '2019_08_19_000000_create_failed_jobs_table', 2),
(19, '2023_05_12_163305_create_dechets_table', 1),
(42, '2019_12_14_000001_create_personal_access_tokens_table', 2),
(43, '2023_05_12_202430_create_users_dechets_table', 2),
(44, '2023_05_16_163156_create_usersapp_table', 2),
(45, '2023_05_18_155344_create_wastes_table', 2),
(46, '2023_06_19_221409_create_posts_table', 2);

-- --------------------------------------------------------

--
-- Structure de la table `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
CREATE TABLE IF NOT EXISTS `password_reset_tokens` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nom` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `nom`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'Wane', 'malickwane26@gmail.com', 'message', '2023-06-21 10:47:33', '2023-06-21 10:47:33'),
(2, 'Wane', 'papiyade@gmail.com', 'aaaa', '2023-06-21 10:52:18', '2023-06-21 10:52:18'),
(5, 'djily diaw', 'ptoure@gmail.com', 'merci beaucoup', '2023-07-03 23:32:47', '2023-07-03 23:32:47'),
(6, 'Mustafa', 'mustef@gmail.com', 'merci', '2023-07-07 15:34:20', '2023-07-07 15:34:20'),
(7, 'Mouhamadou Moustapha Diaw', 'diawmoha79@yahoo.com', 'thanks', '2023-07-24 13:28:44', '2023-07-24 13:28:44'),
(8, 'bachir kida', 'bachir@gmail.com', 'Merci pour le service', '2023-07-26 10:10:14', '2023-07-26 10:10:14');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `statutuser` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `statutuser`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'malick', 'malickwane26@gmail.com', NULL, 'user', '$2y$10$uOlVZcLeBvH6bR6RYNUskOliKcGdUi9aG0wJZGcPugLvkmHXKCHRm', NULL, '2023-06-21 11:31:06', '2023-06-21 11:31:06'),
(2, 'admin', 'admin@gmail.com', NULL, 'admin', '$2y$10$e2Dy9EMPF1.D3BLWOBK35uXjE2HlC62do.qPXEE6oOp2RoATGOXhG', NULL, '2023-06-21 11:51:52', '2023-06-21 11:51:52'),
(3, 'user', 'user@gmail.com', NULL, 'user', '$2y$10$BxI.dgzRYctjAU3p0.zSzucVcjCIdpBcVXmBZ2dwmblUJ49mrxiNW', NULL, '2023-06-21 11:52:08', '2023-06-21 11:52:08'),
(4, 'bachir kida', 'bachir@gmail.com', NULL, 'user', '$2y$10$8i7H.uy6IPRNAvARsdx9.OUG0/emgPsifThKT4mTMuXpQwpmwyalW', NULL, '2023-07-26 10:00:22', '2023-07-26 10:00:22'),
(6, 'bachir faye', 'bachir1@gmail.com', NULL, 'user', '$2y$10$3wMZsZ7xRgE/FoyRHvtpD.EFgWpNsHzH3/3jMsZRtJtw2E0XtQ.o6', NULL, '2023-07-26 10:17:58', '2023-07-26 10:17:58'),
(8, 'Khalifa', 'khalifa1@gmail.com', NULL, 'user', '$2y$10$/eg2dviRB9AjFZ50u6Vo6uTzitc8G0GaSB4WwW74.Gy9Vu7osT2Cy', NULL, '2023-07-26 10:46:18', '2023-07-26 10:46:18');

-- --------------------------------------------------------

--
-- Structure de la table `usersapp`
--

DROP TABLE IF EXISTS `usersapp`;
CREATE TABLE IF NOT EXISTS `usersapp` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usersapp_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `usersapp`
--

INSERT INTO `usersapp` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Papii Toure', 'ptoure@gmail.com', '$2y$10$e/dbtWsh6j3dzudZ9q36ZeHSdbuOwB1Hf9mZ9nvSOwtJjZHOo4iaC', 'user', NULL, '2023-06-27 18:11:27', '2023-06-27 18:11:39'),
(2, 'Mustafa', 'mustef@gmail.com', '$2y$10$6X4JQLLMVGLGVwTnFXmSL.L/mY4dqsvbYxzXjcqyHXSQJt/S54mz6', 'user', NULL, '2023-07-07 18:18:10', '2023-07-07 18:18:10'),
(3, 'Rama Sèye', 'Seyeramze@gmail.com', '$2y$10$FoxrizA4I1psP6tdyfyHJOko2KRcl.jgewctOnvVf9fydSA3kzNtO', 'user', NULL, '2023-07-24 11:52:59', '2023-07-24 11:52:59'),
(4, 'ryad', 'dRyad@supdeco.edu.sn', '$2y$10$vKISulHnYU5EMVndYpeZc.9Ltsq2tcL8brdYZVHN7lfUuHbX8COoS', 'user', NULL, '2023-07-26 12:54:06', '2023-07-26 12:54:06');

-- --------------------------------------------------------

--
-- Structure de la table `users_dechets`
--

DROP TABLE IF EXISTS `users_dechets`;
CREATE TABLE IF NOT EXISTS `users_dechets` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `dechet_id` bigint(20) UNSIGNED NOT NULL,
  `role` enum('admin','user') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `wastes`
--

DROP TABLE IF EXISTS `wastes`;
CREATE TABLE IF NOT EXISTS `wastes` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` enum('recyclable','hazardous','non-recyclable') COLLATE utf8mb4_unicode_ci NOT NULL,
  `disposal_method` enum('landfill','incineration','recycling') COLLATE utf8mb4_unicode_ci NOT NULL,
  `disposal_cost` decimal(8,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `wastes`
--

INSERT INTO `wastes` (`id`, `name`, `description`, `category`, `disposal_method`, `disposal_cost`, `created_at`, `updated_at`) VALUES
(1, 'Papy Toure', 'dechets en putréfaction', 'recyclable', 'landfill', '0.01', '2023-07-04 08:48:10', '2023-07-04 08:48:39'),
(2, 'djily diaw', 'Dechets de champs', 'non-recyclable', 'landfill', '0.01', '2023-07-07 18:20:47', '2023-07-07 18:20:47'),
(3, 'Papi Toure', 'ff', 'hazardous', 'incineration', '0.11', '2023-07-07 18:21:44', '2023-07-07 18:25:45');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
